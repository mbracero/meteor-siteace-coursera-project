/////
// routing 
/////
Router.configure({
	layoutTemplate: "AppLayout"
});

Router.route('/', function() {
	this.render('navbar', {
		to: "navbar"
	});
	this.render('container', {
		to: "main"
	});
});

Router.route('/website/:_id', function() {
	this.render('navbar', {
		to: "navbar"
	});
	this.render('website_detail', {
		to: "main",
		data: function() {
			return Websites.findOne({_id: this.params._id});
		}
	});
});

/////
// configs 
/////
Accounts.ui.config({
  passwordSignupFields: 'USERNAME_AND_EMAIL'
});

/////
// template helpers 
/////

Template.registerHelper('formatDate', function(date) {
  return moment(date).format('MM-DD-YYYY hh:mm a');
});

Template.registerHelper('hasItems', function(items) {
  return (items && items.length > 0);
});

/*
Template.website_form.helpers({
	invalidRequired:function(field){
		return !$("#" + field).val() || $("#" + field).val().split(" ").join("") === "";
	}
});
*/

// helper function that returns all available websites
Template.website_list.helpers({
	websites:function(){
		var findBy = {}, userFilter;
		if (Session.get("userFilter")) {
			userFilter = _.map(Session.get("userFilter"), function(item){ return new RegExp(item, 'gi'); });
			findBy = {
				$or: [
					{ title: {$in: userFilter} },
					{ description: {$in: userFilter} },
					{ comments: { $elemMatch: { comment: {$in: userFilter} } } }
				]
			};
		}
		return Websites.find(findBy, {sort: {upvotes: -1, downvotes: 1}});
	}
});

Template.website_recommender_modal.helpers({
	recommended_websites:function(){
		var findBy = {_id: -1}, userRecommender, ret;
		if (Session.get("userRecommender")) {
			console.log("userRecommender (" + Session.get("website_id") + ") :: " + JSON.stringify(Session.get("userRecommender"), false, 4));
			userRecommender = _.map(Session.get("userRecommender"), function(item){ return new RegExp(item, 'gi'); });
			findBy = {
				_id: {$not: Session.get("website_id")},
				$or: [
					{ title: {$in: userRecommender} },
					{ description: {$in: userRecommender} }
				]
			};
		}
		ret = Websites.find(findBy, {sort: {upvotes: -1, downvotes: 1}});
		if (ret.count() > 0) {
			$("#website_recommender_modal").modal('show');
		}

		return ret;
	}
});


/////
// template events 
/////

Template.website_search.events({
	"click .js-clear-website-search":function(event){
		$("#keywords").val("");

		Session.set("userFilter", null);
		
		return false;// stop the form submit from reloading the page
	},
	"submit .js-filter-website-search":function(event){
		var keywords = event.target.keywords.value.split(" ");

		Session.set("userFilter", keywords);
		//console.log("Search by : " + keywords);

		return false;// stop the form submit from reloading the page
	}
});

Template.website_item.events({
	"click .js-upvote":function(event){
		// example of how you can access the id for the website in the database
		// (this is the data context for the template)
		var website_id = this._id,
			userRecommender = this.title.split(" ");

		// put the code in here to add a vote to a website!
		Websites.update({_id: website_id}, {$inc: { upvotes: 1 }});

		//console.log("Recommender site by "+JSON.stringify(userRecommender, false, 4));

		Session.set("userRecommender", userRecommender);
		Session.set("website_id", website_id);

		return false;// prevent the button from reloading the page
	}, 
	"click .js-downvote":function(event){

		// example of how you can access the id for the website in the database
		// (this is the data context for the template)
		var website_id = this._id;
		//console.log("Down voting website with id "+website_id);

		// put the code in here to remove a vote from a website!
		Websites.update({_id: website_id}, {$inc: { downvotes: 1 }});

		return false;// prevent the button from reloading the page
	}
})

var saveWebsite = function(data, target) {
	//  put your website saving code in here!
	if(Meteor.user() && data.url !== "" && data.title !== "" && data.description !== "") {
		Websites.insert({
			url: data.url,
			title: data.title,
			description: data.description,
			createdOn: new Date(),
			createdBy: Meteor.user()._id,
			upvotes: 0,
			downvotes: 0,
			comments: []
		});
		// clean fields
		target.url.value = "";
		target.title.value = "";
		target.description.value = "";
		$("#website_form").toggle('slow'); // hide form
	}
}

Template.website_form.events({
	"click .js-toggle-website-form":function(event){
		$("#website_form").toggle('slow');
	}, 
	"submit .js-save-website-form":function(event){

		// here is an example of how to get the url out of the form:
		var url = event.target.url.value.split(" ").join(""),
			title = event.target.title.value.split(" ").join(""),
			description = event.target.description.value.split(" ").join(""),
			template, htmlContent, tempDom, finalTitle, finalDescription;

		Meteor.call('getUrlData', url, function(err,response) {
			if(err) {
				console.log("Error: URL not retrieve. Using user input - " + err.reason);
				saveWebsite({url: url, title: title, description: description}, event.target);
				return;
			}
			//console.log("Result:::: " + JSON.stringify(response, false, 4));
			template = response.content;
			htmlContent = $.parseHTML(template);
			tempDom = $('<output>').append(htmlContent);

			finalTitle = (
					$('title', tempDom) && $('title', tempDom).text()
				) || title;

			finalDescription = (
					$('meta[name="description"]', tempDom) && $('meta[name="description"]', tempDom).attr("content")
				) || description;

			saveWebsite({url: url, title: finalTitle, description: finalDescription}, event.target);
		});

		return false;// stop the form submit from reloading the page

	}
});

Template.website_comment_form.events({
	"click .js-toggle-website-comment-form":function(event){
		$("#website_comment_form").toggle('slow');
	}, 
	"submit .js-save-website-comment-form":function(event){

		var comment = event.target.comment.value,
			website_id = this._id,
			currentComments = this.comments || [],
			website_comment = {comment: comment, createdOn: new Date()};

		//console.log("The url they entered is: "+url);
		
		//  put your website saving code in here!
		if(comment !== "") {
			currentComments.unshift(website_comment);
			Websites.update({_id: website_id}, {$set: { comments: currentComments }});
			
			$("#website_comment_form").toggle('slow', function() {
				//Router.go('/');
				Session.set("userRecommender", event.target.comment.value.split(" "));
				Session.set("website_id", website_id);
				event.target.comment.value = "";
			}); // hide form
		}

		return false;// stop the form submit from reloading the page

	}
});

Template.website_recommender_modal.events({
	"click .js-recommender-item":function(event){
		//console.log("Hide modal");
		$("#website_recommender_modal").modal('hide');
	}
});
