Future = Npm.require('fibers/future');

// start up function that creates entries in the Websites databases.
Meteor.startup(function () {
  // code to run on server at startup
  if (!Websites.findOne()){
  	console.log("No websites yet. Creating starter data.");
  	  Websites.insert({
  		title:"Goldsmiths Computing Department", 
  		url:"http://www.gold.ac.uk/computing/", 
  		description:"This is where this course was developed.", 
  		createdOn:new Date(),
      upvotes: 0,
      downvotes: 0,
      comments: []
  	});
  	 Websites.insert({
  		title:"University of London", 
  		url:"http://www.londoninternational.ac.uk/courses/undergraduate/goldsmiths/bsc-creative-computing-bsc-diploma-work-entry-route", 
  		description:"University of London International Programme.", 
  		createdOn:new Date(),
      upvotes: 0,
      downvotes: 0,
      comments: []
  	});
  	 Websites.insert({
  		title:"Coursera", 
  		url:"http://www.coursera.org", 
  		description:"Universal access to the world’s best education.", 
  		createdOn:new Date(),
      upvotes: 0,
      downvotes: 0,
      comments: []
  	});
  	Websites.insert({
  		title:"Google", 
  		url:"http://www.google.com", 
  		description:"Popular search engine.", 
  		createdOn:new Date(),
      upvotes: 0,
      downvotes: 0,
      comments: []
  	});
  }

  Meteor.methods({
    getUrlData: function (url) {
      var fut = new Future();

      //console.log('on server, welcome called with name: ', url);
      if(url==undefined || url.length<=0) {
          throw new Meteor.Error(404, "Please enter url");
      }

      /**
      CALL SYNC
      https://themeteorchef.com/snippets/using-the-http-package/#tmc-synchronous-http-requests-on-the-server
        Here, we make call HTTP.call() without a callback function as the final parameter. Without this,
      Meteor knows we want to run our request synchronously and will simply return either the error or response from the other server.
      **/
      //return HTTP.call( 'GET', url, {});

      /****************************************************************************************************
      ****************************************************************************************************
      ****************************************************************************************************/

      /**
      CALL ASYNC
      http://stackoverflow.com/questions/24743402/how-to-get-an-async-data-in-a-function-with-meteor
      **/
      HTTP.call( 'GET', url, function( error, response ) {
        if ( error ) {
          console.log( "Error:: " + error );
          fut.throw(error);
        } else {
          console.log( "Response:: " + JSON.stringify(response, false, 4) );
          fut.return(response);
        }
      });

      return fut.wait();
    }
  });
});