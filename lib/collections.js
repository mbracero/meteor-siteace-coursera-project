Websites = new Mongo.Collection("websites");

Websites.allow({
	insert:function(userId, document) {
		return (userId === document.createdBy);
	},
	update: function (userId, doc, fields, modifier) {
		return true;
	}
});
